/*
	What is Javascript (JS)?
		Javascript is a scripting programming language that enables us to make interactive web pages.

		Comments
			in JS there are two ways to add comments:
				// or ctrl + / - creates a single line comment

				ctrl + shift + / - creates a multiline comment.

			MULTILINE COMMENTS allows us to add multiple lines in a single comment.

*/
			// while SINGLE LINE COMMENT - allows only for a single line of comment

console.log("Hello, World");

/*
	console.log() allows us to show/display data in our console. The console is a part of our browser with which we can use to display data.
*/

console.log("Jam");
console.log("Pizza");
/*
	Statement and Syntax

	Statements are instructions/expressions we add to our script/program which will then be communicated to our computers. Our computers will then be able to interpret these instructions and perform the task accordingly.

	Example:
	console.log("Jam");
		The computer is using JS to:
			"Display/log this data in the console."

	Most programming languages end their statements in semicolon(;)

	Syntax, in programming, is a set of rules that describes how statements or instructions are properly made/constructed.

	For your programs/script to work, we should be able to follow a certain set of rules. How to write properly a code.
	correct:
		console.log(<data>);
	wrong:
		()console.log
*/
/*
	Variables

	In HTML, elements are containers for text and other elements.

	In HavaScript, variables are containers of data.

	This will then allow us to save data within our script or program.

	To create a variable, we use the let keyword and the assignment operator (=).

	Syntax:

	let variableName = "data";
*/

let name = "Jessa Mae Loresto";

// log the value of the variable in the console:
console.log(name);

// save numbers in variables:

let num = 5;
let num2 = 10;

// log the values of the variables in the console:
console.log(num);
console.log(num2);

// When console logging variables, the name of the variables will not be shown what will be shown/displayed are the contained within variables.

// You could also check/display the values of multiple variables:
// console.log(variable1,variable2);

console.log(name,num,num2);

// We cannot display the value of a variable that is not yet created/declared
// In fact, that will result in an error. (not defined)
// console.log(name2);

let myVariable;
/*
	You can actually create variables without providing an initial value. However, that variable will be assigned as "undefined". Because we don't know the value yet, because we haven't provided a value yet. You can actually add the value later.
*/
console.log(myVariable); //result:undefined
/*
	Creating variables is actually 2 steps:

	1. Declaration - It is the declaration/creation of the variable with either the let or const keyword.

	2. Initialization - is when we provide an initial value to our variable.

	Declaration		Initialization
	let myVar 	=	"initial value"

*/

// You can update/assign a value to a variable after it has been declared.
myVariable = "new value";
console.log(myVariable);

let bestFinalFantasy = "Final Fantasy X";
console.log(bestFinalFantasy);

// You can update the value of a variable declared using the let keyword
bestFinalFantasy = "Final Fantasy 7";
console.log(bestFinalFantasy);

// We cannot update with the let keyword.
// We cannot create another variable with the same name
// let bestFinalFantasy = "Final Fantasy 6";
// console.log(bestFinalFantasy);

/*
	Const
	
	const keyword allows us to create a variable like let, however, the cost variable cannot be updated. Values in a const variable cannot be changed. You also cannot create a const variable without initialization.
*/

const pi = 3.1416;
console.log(pi);
// variable declared with const cannot be updated or reassigned
// pi = 3.15;
// console.log(pi);

// Declare const variable without initialization:
// const plateNum;
//  console(plateNum);

let name2 = "Edward Cullen";
let role = "Supervisor";

role = "Director";
const tin = "1233-1234";
console.log(name2,role,tin);

/*
	Conventions in creating variable/constant names:

	To create a let variable, we use the let keyword, to create a const variable, we use the const keyword.

	Variables declared with let, its value can be updated. Variables declared with const, we cannot change the value.

	Let variables and const variables are usually named in small caps. Because there are other JS keywords that start in capital letter.

	If you want to name your variables with multiple words, we can use camelCase. camelCase is a convention in writing variables by adding the first word in small caps and the following words starting with capital letters.

	Variable names define the values they contain. Name your variables appropriately.
*/

let user = "09266772400"
let carBrand = "Despacito"

// Data Types

/*
	In most programming languages, data is differentiated into different types and we can do different things about this data. For most programming languages, we have to declare the data type of our data before we are able to create the variable and store it.

	However, JS is not strict when it comes to data types.

	There are data types wherein we need to use literals to create them:
	To create strings, we use string literals like '' or ""
	To create objects, we can use object literals like {}
	To create arrays, we can use array literals like []
*/

/*
	Strings

	Strings are a series of alphanumeric characters that create a word, phrase name or anything that is related to creating a text

	Strings are NOT and should not be used for mathematical operations.

	Strings are created with string literals like single quotes('') or double quotes("")
*/

let country = "Philippines";
let province = "Metro Manila";

console.log(province,country);

/*
	You can actually combine strings into a single string with the use of the plus sign or addition operator (+).

	This process is called concatenation.
*/

// In strings, spaces and commas count as characters
let address = province + ", " + country
console.log(address);

let city1 = "Manila";
let city2 = "Copenhagen";
let city3 = "Washington D.C.";
let city4 = "Tokyo";
let city5 = "New York";
let country1 = "Philippines";
let country2 = "U.S.A.";
let country3 = "South Korea";
let country4 = "Japan";

let capital1 = city1 + ", " + country1
let capital2 = city3 + ", " + country2
let capital3 = city4 + ", " + country4

console.log(capital1);
console.log(capital2);
console.log(capital3);

/*Numbers/Integers*/
// Integers are number data which can be used for mathematical operations
// To create a number data, add a numeric character but wih no "" or ''

let number1 = 10;
let number2 = 6;

console.log(number1);
console.log(number2);

/*
	Addition Operator

	(+) when used between 2 number types, it will add both numbers. The result of the addition can also be saved in a variable.
*/

let sum1 = number1 + number2
console.log(sum1);//16

let sum2 = 16 + 4;
console.log(sum2);//20

let numString1 = "30";
let numString2 = "50";
/*These are numeric strings, they are composed of numberic characters but are considered a string because it is surrounded/created with double quotes("")*/

let sumString1 = numString1 + numString2;
console.log(sumString1);
// When numeric strings are used with addition operator(+), it will not add but only combine the strings, it will concatenate

// Note: When a number/integer is added with a numeric string, it results to a concatenation.
let sum3 = number1 + numString2;
console.log(sum3);

/*Boolean (true or false)*/
	/*
		Boolean is usually used for logical operations and if-else conditions.

		When creating a variable that holds a boolean, the variable is usually a yes or a no question.

	*/

	let isAdmin = true;
	let isMarried = true;
	let isMVP = true;

	console.log("Is she married? " + isMarried);
	console.log("Is Curry MVP? " + isMVP);
	console.log("Is he the current admin? " + isAdmin);

/*Arrays*/
	// Arrays are special kind of data type. It is used to store multiple values

	// Arrays can actually store values of different data type, however, this is not a good practice, because there are array methods or ways to manipulate array which might cause a conflict.

	// Arrays are usually used to store multiple values of the same data type.
	// There are only few use cases wherein arrays are used with multiple values of different types.

	// Arrays are created using an array literal or square brackets []
	// Values in an array are separated

	// This is a good array which:
		// The calues have the same data type.
		// The values share the same theme.
	let array1 = ["Goku","Gohan","Goten","Vegeta","Trunks","Broly"];

	// This is an example of bad array:
		// Doesn't make sense why they are grouped together.
		// Having an array with different data types may obstruct to methods which will allow us to manipulate data.
	let	array2 = ["One Punch Man","Saitama",true,5000];

	console.log(array1);
	console.log(array2);

/*Objects*/
	/*
		Objects are another special kind of data type used to mimic real world items/objects.

		It is used to create complex data structure that contain pieces of information that relate to each other.

		Each field in an object is called a property. Each property is separated by a comma.

		Each property is a pair of key: value

		Objects can actually group different data types.

		Each data is given more significance because each data/value has key which defines/describes the data
	*/

	let hero1 = {
		heroName: "One Punch Man",
		realName: "Saitama",
		income: 5000,
		isActive: true,
	}

	let oneDirection = ["Harry", "Zayn", "Louis", "Niall", "Liam"];
	console.log(oneDirection);

	let person1 = {
		firstName: "Jessa Mae",
		lastName: "Loresto",
		isDeveloper: true,
		age: 31
	}
	console.log(person1);

/*Null and Undefined*/

// Null is the explicit absence of data/value. This is done to show that a variable actually contains nothing as opposed to undefined which means that the variable has been declared or create but there was no initial value.
	// Null explicit absence value
	// Undefined there is no value YET

	// Use Cases of Null

// When doing a query or search, there, of course might be a 0 result
	let foundResult = null;

// Undefined is a representation that a variable has been created or declared but there is no initial value, so, we can't quite say what the value is, thus it is undefined.
		
	let sampleVariable;//declaration with no initial value result to undefined

//For undefined, this is normally caused by developers when creating variables that have no data or value associated or initialized with them.

	let person2 = {
		name: "Peter",
		age: 42,
	}

	// Access or display the value of an object's property, we use dot notation:
		//objectName.propertyName
		console.log(person2.name);
		console.log(person2.age);

		// Undefined, because the person2 variable does exist, however there is no property in the object called isAdmin.
		console.log(person2.isAdmin);

/*Functions*/

/*
	Functions in JS, are linees/blocks of code that tell our device/application to perform a certain task when called or invoked.

	Functions are reusable pieces of code with instructions/statements which can be used over and over again just so long as we call or use it.

	You can think of functions as programs within a program.
*/
	console.log("Good Afternoon, Everyone! Welcome to my application!");

	// functions are created by declaring the function using the function keyword.
	function greet(){
		console.log("Good Afternoon, Everyone! Welcome to my application!");
	}

	greet();
	greet();

	favoriteFood = "pizza"
	console.log(favoriteFood);

	let sum4 = 150 + 9;
	console.log(sum4);

	let product1 = 100 * 90;
	console.log(product1);

	let isActive = true;
	console.log("Is the user active? " + isActive);

	let faveResto = ["Vikings", "Sumo Niku", "Domino's" ,"Mangan", "Yellow Cab"];

	console.log(faveResto);

	let favorite = {
		firstName: "Yu Wei",
		lastName: "Shao",
		stageName: "Ivy Shao",
		birthDay: "September 21, 1989",
		age: "32",
		bestAlbum: "Drizzle",
		bestSong: "Just Do What I Want",
		bestTvShow: "The Perfect Match",
		bestMovie: "Stand By Me",
		isActive: true,
	}


// Continuation of Functions

	// Parameters and Arguements
	// "name" is called a parameter
	// Parameter acts as a named variable/ container that exists ONLY in the function. This is used to store information to act as a stand-in or the container of the value passed into the function as an arguement.

	function printName(name) {
		console.log(`My name is ${name}`)
	};
	// console.log(name);

	// Data passed in the function: arguement
	// Representation of the arguement within the function: paramenter
	printName("Jake");

	function displayNum(number) {
		console.log(number)
	};

	displayNum(3000);
	displayNum(3001);

		function displayMessage(message) {
		console.log(message)
	};

	displayMessage("Javascript is fun!");

// Multiple Parameters and Arguements
	// Function can not only receive a single arguement but it can also receive multiple arguements as long as it matches the number of parameters in the function.

	function displayFullName(firstName, lastName, age){
		console.log(`${firstName} ${lastName} is ${age}`)
	};

	displayFullName("Jessa Mae", "Loresto", "31");

// return keyword
	// return keyword is used so that a function may return a value
	// it also stops the process of the function any other instruction after the return keyword

	function createFullName(firstName, middleName, lastName){
		return `${firstName} ${middleName} ${lastName}`
		console.log("I will no,longer ru because the function's value/ result has been returned.")
	};

	let fullName1 = createFullName("Tom", "Cruise", "Mapother");
	console.log(fullName1);